import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader


Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
    theme: {
        themes : {

            light: {
                primary: '#333333',
                secondary: '#282828', 
                white: '#ffffff',
                orange: '#FC6D28', 
                darkgrey: '#707070', 
                complete: '#2BFC83',
                inprogress: '#FF8936', 
                incomplete: '#FF1212', 
                lightgrey: '#cacaca'
            }
        }    
    }
});
